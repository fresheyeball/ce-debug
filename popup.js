init();

let linkedinData = null;
let companyData = null;
let chats = [];
let ctaData = null;
var counts = [];
let body = null;
let sig = null;
let existing = null;
let companyDataTimerId = null;
let companyDataTimeLimit = 45000;
let seenUrls = [];

var NOEVENT = "NOEVENT";

const appendAbout = "about/?viewAsMember=true"

let console_ = {
  log: (...args) => console.log(...args),
  time: (...args) => console.time(...args),
  timeEnd: (...args) => console.timeEnd(...args),
};

// let console_ = { log: () => {}, time: () => {}, timeEnd: () => {} }

let log = console_.log;


let TABURL = null;

// inject linkedinScraper.js into linkedin page
chrome.tabs.query({ currentWindow: true, active: true }, (tabs) => {
  if (
    tabs.length &&
    tabs[0].url &&
    new URL(tabs[0].url).hostname == "www.linkedin.com"
  ) {
    TABURL = tabs[0].url;
    log("starting linked in scraper");
    document.querySelector("#create-loading").style.display = "block";
    chrome.tabs.executeScript(tabs[0].id, { file: "linkedinScraper.js" });
  } else {
    log("show create div from query");
    showCreateChatDiv();
  }
});

// listen for scraped data from linkedinScraper.js
chrome.runtime.onMessage.addListener((message) => {
  if (message && ["log", "time", "timeEnd"].includes(message.type)) {
    console_[message.type](message.msg);
  }
  if (message && message.type == "URL_SEEN"){
    log("url seen... ", message.msg);
    seenUrls = seenUrls.concat(message.msg);
  }
  if (message && message.type == "checking-positions"){
    document.querySelector("#loading-name-location .material-icons").style.opacity = 1;
  }
  if (message && message.type == "linkedinData") {
    log("linkedinData 1");
    linkedinData = message.data;
    // fill Create Prospect Chat with scraped LinkedIn data
    document.querySelector("#loading-name-location .material-icons").style.opacity = 1;
    fillLinkedInData(message.data);

    log("linkedinData", linkedinData);

    // if there were errors during scraping, report them to /api/chrome/alert API endpoint
    if (message.data.scrapingErrors.length) {
      reportErrors(
        message.data.url,
        message.data.scrapingErrors,
        message.data.linkedInVersion
      );
      log("show create chat from scrapingErrors");
      showCreateChatDiv();
    } else {
      log("checking for duplicates");
      checkForDuplicate(message.data);
      getEmailStatus(message.data.LinkedInURL);
    }

    if(linkedinData.CurrentCompany){
      gotCompanyData(linkedinData.CurrentCompany);
    }

    // enable / disable Create Chat Link button based on fields
    validate();
  }
  if (message && message.type == "companyData") {
    companyData = message.data;
    log("companyData", companyData);
    gotCompanyData(companyData);
  }
  if( message && message.type == "requestCompanyData" ){
    if(companyData){
      log("early company data", companyData);
      gotCompanyData(companyData);
    }else{
      gotDataTimeout();
    }
  }
});


function gotCompanyData(companyData){
    clearTimeout(companyDataTimerId);
    if(!chats[0] && companyData){
      return;
    }
    Promise.all([
      fetch(`${BASE_URL}/${CTAS}${encodeURI(chats[0].Id)}`, {
        method: "get",
        mode: "cors",
      })
      .then(checkResponse)
      .then(x => {
        document.querySelector("#loading-cta .material-icons").style.opacity = 1;
        return x;
      }),
      fetch(`${BASE_URL}/${REASONS}`, {
        method: "post",
        mode: "cors",
        body: JSON.stringify({
          Company: companyData,
          Candidate: linkedinData,
        }),
      })
      .then(checkResponse)
      .then(x => {
        document.querySelector("#loading-reaching .material-icons").style.opacity = 1;
        return x;
      })
    ])
      .then(function (res) {
        ctaData = res[0];
        console.log("at reason time", chats[0], res[1]);
        if(chats[0].EventType === "applicant_screen"){
          res[1].Reasons = [
            {
              Name: "Default",
              Copy: "I've had a chance to review your application, and I'd like to set up some time to chat briefly over messaging."
            }
          ]
          console.log("reason hijack", res[1])
        }
        setTimeout(() => showChatLinkDiv(chats[0], res[0], res[1]), 500);
      })
      .catch(function (error) {
        log(error);
      });
};

// initialize app
function init() {
  // set location of Login Link according to BASE URL in config.js
  document.querySelector("#loginLink").href = `${BASE_URL}/login`;
  initListeners();
  getJobs();
}

function buildBody() {
  if (document.querySelector("#chatLinkDiv").style.display === "") {
    return;
  }
  const seps = { email: "%0D%0A", copy: "\n" };
  const buildMessage = (proc, sep) =>
    [
      document.querySelector("#select-greeting").value,
      document.querySelector("#select-reason").value,
      document.querySelector("#select-highlight").value,
      document.querySelector("#select-action").value,
    ]
      .filter((x) => x !== "")
      .map(proc)
      .concat([sig.map(proc).join(sep)])
      .join(sep + sep);

  body = {
    email: document.querySelector("#template-address").value,
    subject: encodeURI(document.querySelector("#template-subject").value),
    copyBody: buildMessage(x => x, seps.copy),
    emailBody: buildMessage(encodeURIComponent, seps.email),
  };
  console.log("body", body);
  document.querySelector(
    "#copy-mailto"
  ).href = `mailto:${body.email}?subject=${body.subject}&body=${body.emailBody}`;
}

// initialize listeners
function initListeners() {
  // listen for changes in inputs and revalidate on each change
  document.querySelectorAll("input").forEach((input) => {
    input.addEventListener("keyup", validate);
    input.addEventListener("change", validate);
    input.addEventListener("input", buildBody);
  });

  // listen for changes in selects and revalidate on each change
  document.querySelectorAll("select").forEach((select) => {
    select.addEventListener("change", validate);
    select.addEventListener("change", buildBody);
  });

  // listen for click on Copy Link buttons
  document
    .querySelector("#copyLink")
    .addEventListener("click", copyLink("chatLink"));
  document
    .querySelector("#copyEmail")
    .addEventListener("click", copyLink("template-address"));
  document
    .querySelector("#copySubject")
    .addEventListener("click", copyLink("template-subject"));
  document.querySelector("#copyBody").addEventListener("click", copyBody);

  // listen for click on Create Link & Copy button
  document
    .querySelector("#createChatLinkBtn")
    .addEventListener("click", createLinkAndCopy);
  document.querySelector("#Event").addEventListener("change", showEventMessage);
  showEventMessage();

  document
    .querySelectorAll(".close-button")
    .forEach((btn) => btn.addEventListener("click", () => window.close()));

  document
    .querySelector(".back-button").addEventListener("click", backToEdit);
}

var dbEventType;

function backToEdit(){
  log("backToEdit", chats);
  document
    .querySelector("#loadingDiv").style.display  = "none";
  document
    .querySelector("#chatLinkDiv").style.display = "none";
  document
    .querySelector("#createChatDiv").style.display = "block";
  document
    .querySelector("#edit-header").textContent = "Edit Chat";
  document
    .querySelector("#copiedAfterCreateConfirmation").style.display = "none";
  document
    .querySelector("#create-loading").style.display = "none";
  document
    .querySelector("#template-loading").style.display = "block";
  document
    .querySelector("#loading-prospect").style.display = "block";
  document
    .querySelectorAll("#template-loading .material-icons").forEach(x => x.style.opacity = 0);

  dbEventType = chats[0].EventType;

  document.querySelector("#PhoneNumber").value = chats[0].PhoneNumber;
  document.querySelector("#Notes").value = chats[0].Notes ? chats[0].Notes : "";
  const job = document.querySelector("#Job")
  job.value = chats[0].JobId;
  document.querySelector("#createChatLinkBtn").textContent = "Save & Continue";
  document.querySelector("#Event").value = chats[0].EventType ? chats[0].EventType : NOEVENT;
  document.querySelector("#FirstName").value = chats[0].CandidateName.First;
  document.querySelector("#LastName").value = chats[0].CandidateName.Last;
  document.querySelector("#email").value = chats[0].Email;
  document.querySelector("#CurrentPosition").value = chats[0].CurrentPosition;
  document.querySelector("#CurrentLocation").value = chats[0].CurrentLocation;
  document.querySelector("#LinkedInURL").value = chats[0].LinkedInURL;
  document.querySelector("#Notes").value = chats[0].Notes;

  getCounts(chats[0].JobId);

  if(chats[0].Identifier){
    const special = document.createElement("option");
    special.textContent = chats[0].Identifier;
    special.value = chats[0].Identifier;
    const evt = document.querySelector("#Event");
    evt.appendChild(special);
    evt.value = special.value;
    evt.disabled = true;
  }

  console.log("==>", chats[0]);
  validate();
  tippy('i[data-title]', { content: ref => ref.getAttribute('data-title') })


}

function toggleDisplay(elm) {
  if (elm.style.display == "none") {
    return (elm.style.display = "block");
  }
  elm.style.display = "none";
}

const NEW_EMAIL = "Add New Email";

function generateEmailDropdown(email, emails) {
  const options = document.getElementById("emails");

  function addOption(s) {
    const option = document.createElement("option");
    option.value = s;
    option.text = s;
    options.add(option, null);
  }

  addOption(email);
  addOption(NEW_EMAIL);

  for (i = 0; i < emails.length; i++) {
    addOption(emails[i]);
  }

  options.addEventListener("input", (e) =>
    selectEmailOption(options, e.target.value)
  );
  validate();
}

function selectEmailOption(options, email) {
  if (email == NEW_EMAIL) {
    toggleDisplay(document.getElementById("email"));
    options.parentNode.parentNode.classList.add("with-dropdown");
  } else {
    document.getElementById("email").style.display = "none";
    options.parentNode.parentNode.classList.remove("with-dropdown");
  }
  validate();
}

// get emails from pipl
function getEmails(linkedinURL) {
  const originalEmail = document.getElementById("email");
  const loadingElement = document.getElementById("getting-emails");
  const gotEmails = document.getElementById("got-emails");
  const findEmailLink = document.getElementById("find-email");

  toggleDisplay(originalEmail);
  toggleDisplay(loadingElement);
  toggleDisplay(findEmailLink);

  fetch(`${BASE_URL}/${FIND_EMAILS}${encodeURI(linkedinURL)}`, {
    method: "get",
    mode: "cors",
  })
    .then(checkResponse)
    .then(function (response) {
      toggleDisplay(loadingElement);
      if (response.Found) {
        generateEmailDropdown(response.PreferredEmail, response.PossibleEmails);
        toggleDisplay(gotEmails);
        findEmailLink.className = "emails-found-grey";
        findEmailLink.textContent = `${
          response.PossibleEmails.length + 1
        } email${response.PossibleEmails.length ? "s" : ""} found`;
        toggleDisplay(findEmailLink);
        restoreState();
      } else {
        toggleDisplay(originalEmail);
        findEmailLink.textContent = "No emails found. No credits used.";
        findEmailLink.className = "emails-found-grey";
        delete findEmailLink["href"];
        toggleDisplay(findEmailLink);
      }
    })
    .catch(function (error) {
      log(error);
    });
}

// get email status
function getEmailStatus(linkedinURL) {
  const findEmailLink = document.getElementById("find-email");

  fetch(`${BASE_URL}/${EMAIL_STATUS}`, {
    method: "get",
    mode: "cors",
  })
    .then(checkResponse)
    .then(function (response) {
      log(response);
      if (!response.SignedUp) {
        findEmailLink.textContent = "Sign up for Email Discovery";
        findEmailLink.className = "emails-found-grey";
        return;
      }
      if (response.SignedUp && response.RemainingCredits < 1) {
        findEmailLink.textContent = "No email credits remaining. Buy credits";
        findEmailLink.className = "emails-found-grey";
        return;
      }
      if (response.SignedUp && response.RemainingCredits > 0) {
        findEmailLink.textContent = "Find Email";
        function listen() {
          findEmailLink.removeEventListener("click", listen);
          getEmails(linkedinURL);
        }
        findEmailLink.addEventListener("click", listen);
        return;
      }
    })
    .catch(function (error) {
      log(error);
    });
}

// show Create Chat div
function showCreateChatDiv() {
  document.querySelector("#loading-prospect").style.display = "block";
  document.querySelector("#loadingDiv").style.display       = "none";
  document.querySelector("#createChatDiv").style.display    = "block";
  setupStateListeners();
  tippy('i[data-title]', { content: ref => ref.getAttribute('data-title') })
}

const getSubjectLine = (jobTitle, employerName, eventType) => {
  switch(eventType){
    case "applicant_screen": return `${employerName} next steps - Invitation to chat over messaging`
    case "prospecting":      return `Opportunity at ${employerName} - Invitation to private event`
    case "sourcing":         return getSubjectLine(jobTitle, employerName, "prospecting")
    default:                 return `${jobTitle} opportunity at ${employerName}`
  }
}

const getSignature = (leadRecruiter) => [
  "Regards" + (leadRecruiter ? "," : ""),
  leadRecruiter,
];


// show Chat Link div
function showChatLinkDiv(chat, { CTAs }, { Reasons }) {
  document.querySelector("#createChatDiv").style.display = "none";
  document.querySelector("#loadingDiv").style.display = "none";

  document.querySelector(".back-button").textContent =
    chat.EventId ? "Edit chat or change event"
                 : "Edit chat or add to event";

  document.querySelector("#chatLink").value = chat.URL;
  document.querySelector("#chatLinkDiv").style.display = "block";

  document.querySelector("#template-address").value = chat.Email;
  document.querySelector("#template-subject").value = getSubjectLine(
    chat.RoleTitle,
    chat.EmployerName,
    chat.EventType
  );

  const hey = `Hey ${chat.CandidateName.First},`;
  document.querySelector("#hey").value = hey;
  document.querySelector("#select-greeting").value = hey;
  document.querySelector("#greeting-preview").textContent = hey;

  const reasons = document.querySelector("#select-reason");
  reasons.innerHTML = "";
  Reasons.forEach((reason) => {
    const r = document.createElement("option");
    r.textContent = reason.Name;
    r.value = reason.Copy;
    reasons.appendChild(r);
  });
  const updateReasonPreview = () => {
    if(Reasons.length > 0 || chat.EventType === "applicant_screen"){
      document.querySelector("#reason-preview").textContent = reasons.value;
    }else{
      const expLink = document.createElement("a");
      expLink.id = "exp-link";
      expLink.textContent = "Configure your employer Linkedin url";
      expLink.target = "_blank";
      expLink.href = `${BASE_URL}/${EXP_B}/${chat.ExperienceId}`;
      const preview = document.querySelector("#reason-preview")
      preview.innerHTML = "";
      preview.appendChild(expLink);
    }
  }
  reasons.addEventListener("input", updateReasonPreview);
  updateReasonPreview();

  document.querySelector("#highlight").value = chat.Highlights;
  document.querySelector("#highlight").textContent = chat.ExperienceName;
  document.querySelector("#select-highlight").value = chat.Highlights;
  if (chat.EventType === "applicant_screen"){
    document.querySelector("#highlight-preview").textContent = "";
    document.querySelector("#select-highlight").value = "";
  }else if (chat.Highlights) {
    document.querySelector("#highlight-preview").textContent = chat.Highlights;
  } else {
    const expLink = document.createElement("a");
    expLink.id = "exp-link";
    expLink.textContent = "Configure your company highlight";
    expLink.target = "_blank";
    expLink.href = `${BASE_URL}/${EXP_CI}/${chat.ExperienceId}`;
    const preview = document.querySelector("#highlight-preview")
    preview.innerHTML = "";
    preview.appendChild(expLink);
  }

  const ctas = document.querySelector("#select-action");
  ctas.innerHTML = "";
  CTAs.forEach((cta) => {
    const c = document.createElement("option");
    c.textContent = cta.Name;
    c.value = cta.Copy;
    c.CTA = cta.CTA;
    ctas.appendChild(c);
  });
  const updateCTAPreview = () =>
    (document.querySelector("#action-preview").textContent = ctas.value);
  ctas.addEventListener("input", updateCTAPreview);
  updateCTAPreview();

  sig = getSignature(chat.LeadRecruiter);
  document.querySelector("#signature").value = "sig";
  document.querySelector("#select-signature").value = "sig";
  document.querySelector("#signature-preview").innerHTML = `${sig[0]}
       <br/>
       ${sig[1]}
      `;

  if (existing) {
    document.querySelector(
      "#chatExists"
    ).innerHTML = `A chat appears to already exist with ${
      document.querySelector("#FirstName").value
    } ${
      document.querySelector("#LastName").value
    }. <br> To edit this chat, please visit <a href='${BASE_URL}/app/chats' target='_blank'>Chatstrike.</a>`;
    document.querySelector("#chatExists").style.display = "inline-block";
    document.querySelector(
      "#chatLinkHeader"
    ).textContent = `Chat Already Exists`;
  }

  buildBody();
}

// show Login div
function showLoginDiv() {
  document.querySelector("#createChatDiv").style.display = "none";
  document.querySelector("#loadingDiv").style.display = "none";
  document.querySelector("#chatLinkDiv").style.display = "none";
  document.querySelector("#loginDiv").style.display = "block";
}

const fillDataIfPrudent = (id, val) => {
  const elm = document.getElementById(id);
  if (elm && !elm.value) {
    elm.value = val;
  }
};

// fill Create Prospect Chat with scraped LinkedIn data
function fillLinkedInData(data) {
  log("filling data");
  fillDataIfPrudent("FirstName", data.FirstName);
  fillDataIfPrudent("LastName", data.LastName);
  fillDataIfPrudent("LinkedInURL", data.LinkedInURL);
  fillDataIfPrudent("CurrentPosition", data.CurrentPosition);
  fillDataIfPrudent("CurrentLocation", data.CurrentLocation);
  const name = document.getElementById("FirstName").value + " " + document.getElementById("LastName").value;
  document.getElementById("chat-url").textContent = `Custom Link for ${name}`;
}

// enable / disable Create Chat Link button based on fields
function validate() {
  const emails = document.getElementById("emails");
  const selectedEmail = emails.options[emails.selectedIndex];
  const emailValue = document.getElementById("email").value;
  if (
    document.querySelector("#FirstName").value &&
    document.querySelector("#LastName").value &&
    document.querySelector("#Event").value &&
    document.querySelector("#Job").value
  ) {
    const isInputValid =
      (emailValue && isValidEmail(emailValue)) || emailValue == "";
    if (
      // they never clicked "get emails" but filled out the input right
      (isInputValid && !selectedEmail) ||
      // email input is valid and select is to new email
      (isInputValid && selectedEmail.text == NEW_EMAIL) ||
      // email input is valid and we don't have found emails
      (isInputValid && emails.options.length == 0) ||
      // we have a found email
      (selectedEmail && selectedEmail.text != NEW_EMAIL)
    ) {
      document.querySelector("#createChatLinkBtn").disabled = false;
    } else {
      document.querySelector("#createChatLinkBtn").disabled = true;
    }
  } else {
    document.querySelector("#createChatLinkBtn").disabled = true;
  }
}

// check if email is valid email
function isValidEmail(email) {
  let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(String(email).toLowerCase());
}

const toaster = {};
const toasttime = 1500;

// copies link to clipboard
function copyLink(id) {
  return () => {
    navigator.clipboard.writeText(document.getElementById(id).value);
    const conf = document.querySelector("#copiedConfirmation-" + id);
    conf.style.display = "block";
    conf.style.opacity = "100%";
    setTimeout(() => {
      conf.style.opacity = "0";
      clearTimeout(toaster[id]);
      toaster[id] = setTimeout(() => {
        conf.style.display = "none";
        delete toaster[id];
      }, 1000);
    }, toasttime);
  };
}

function copyBody() {
  navigator.clipboard.writeText(body.copyBody);
  const conf = document.querySelector("#copiedConfirmation-body");
  conf.style.display = "block";
  conf.style.opacity = "100%";
  const reasons = document.querySelector("#select-reason");
  const greetings = document.querySelector("#select-greeting");
  const highlight = document.querySelector("#select-highlight");
  const signature = document.querySelector("#select-signature");
  const ctas = document.querySelector("#select-action");
  fetch(`${BASE_URL}/${AI_LOG}`, {
    method: "post",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      ChatId: chats[0].Id,
      CTA: ctaData.CTAs.filter(c => c.Name === ctas.options[ctas.selectedIndex].text)[0].CTA,
      Reasons: reasons.options[reasons.selectedIndex].text,
      Greeting: greetings.options[greetings.selectedIndex].text,
      Highlight: highlight.options[highlight.selectedIndex].text,
      Signature: signature.options[signature.selectedIndex].text,
    })
  })
  setTimeout(() => {
    conf.style.opacity = "0";
    clearTimeout(toaster["body"]);
    toaster["body"] = setTimeout(() => {
      conf.style.display = "none";
      delete toaster["body"];
    }, 1000);
  }, toasttime);
}

function formatDate(date) {
  date = new Date(date);
  let zone = date
    .toLocaleTimeString("en-us", { timeZoneName: "short" })
    .split(" ")[2];
  let time = date
    .toLocaleString("en-us", { timeStyle: "short", dateStyle: "short" })
    .split(", ");
  return (
    time[1].split(" ")[0] +
    time[1].split(" ")[1].toLowerCase() +
    " " +
    zone +
    " on " +
    time[0].split("/")[0] +
    "/" +
    time[0].split("/")[1]
  );
}

function showEventMessage() {
  let eventsDropdown = document.querySelector("#Event");
  if (eventsDropdown.value == NOEVENT) {
    document.querySelector("#EventMessage").textContent =
      "This chat starts once they click their personal link";
    return;
  }else if(eventsDropdown.value){
    setEventMessageCount();
    return;
  }
  // if (eventsDropdown.value) {
  //   let selected = eventsDropdown.options[eventsDropdown.selectedIndex];
  //   let start = formatDate(selected.dataset.start);
  //   // let end = formatDate(selected.dataset.end);
  //   document.querySelector(
  //     "#EventMessage"
  //   ).textContent = `This chat can start after ${start}`;
  //   return;
  // }
  document.querySelector("#EventMessage").textContent = "";
}

function getCounts(role_id){
  fetch(`${BASE_URL}/${COUNTS}?role_id=${role_id}`, {
    credentials: "include",
    method: "get",
    mode: "cors",
  })
    .then(checkResponse)
    .then(function (cts){
      counts = cts;
      showEventMessage();
    });
}

function setEventMessageCount(){
  console.log({counts});
  if(!document.querySelector("#Job").value){
    return;
  }
  const msg = document.querySelector("#EventMessage");
  const evt = document.querySelector("#Event").value;
  var count = 0
  if(counts && counts.length){
     const t = counts.filter(x => x && x.EventType && x.EventType === evt);
     if(t[0]){
       count = t[0].Count
     }
  }
  msg.innerHTML = null;
  var a = document.createElement("a");
  a.target = "_blank";
  var t1 = null;
  var t2 = null;
  const setMessageText = (m1, l, m2) => {
    a.textContent = l ? l : "";
    t1 = document.createTextNode(m1);
    t2 = document.createTextNode(m2 ? m2 : "");
  };
  a.href = `${BASE_URL}/app/events/settings/${evt.replace('_','-')}/new`;
  switch(count){
    case 0:
      setMessageText("Alert: No ", "upcoming events", " for them to join");
      break;
    case 1:
      setMessageText("Only 1 upcoming event. ", "Add more", " to raise attendance");
      break;
    case 2:
      setMessageText("Only 2 upcoming events. ", "Add more", " to raise attendance");
      break;
    case 3:
      setMessageText("Only 3 upcoming events. ", "Add more", " to raise attendance");
      break;
    default:
      setMessageText(`They can choose from one of ${count} upcoming events`);
      break;
  }
  msg.appendChild(t1);
  msg.appendChild(a);
  msg.appendChild(t2);
}

// get jobs from API and fill the dropdown with them
function getJobs() {
  console.log("Getting jobs...");
  fetch(`${BASE_URL}/${JOBS}`, {
    credentials: "include",
    method: "get",
    mode: "cors",
  })
    .then(checkResponse)
    .then(function (jobs) {
      // sort jobs in ascending order by their titles
      jobs.sort((a, b) => (a.Title > b.Title ? 1 : -1));

      // fill dropdown with jobs
      let jobDropdown = document.querySelector("#Job");
      jobs.forEach((job) => {
        let option = document.createElement("option");
        option.textContent = job.Title;
        option.value       = job.Id;
        option.setAttribute("CompanyLinkedIn", job.CompanyLinkedIn);
	option.setAttribute("ExperienceId",    job.ExperienceId);
        jobDropdown.appendChild(option);
      });

      const getCounts_ = () => {
        const v = jobDropdown.options[jobDropdown.selectedIndex].value;
        if(v){
          getCounts(v);
        }
      };

      getCounts_();
      jobDropdown.addEventListener("change", getCounts_);

      validate();
    })
    .catch(function (error) {
      log(error);
    });
}

function getEmail() {
  const emails = document.getElementById("emails");
  const selected = emails.options[emails.selectedIndex];
  if ((selected && selected.text == NEW_EMAIL) || !selected) {
    return document.getElementById("email").value;
  }
  return selected.text;
}

function getPossibleEmails() {
  const email = document.getElementById("email").value;
  const options = Array.apply(null, document.getElementById("emails").options)
    .map((o) => o.text)
    .filter((o) => o != NEW_EMAIL);
  log(email, options);
  if (email != "") {
    return [email].concat(options);
  }
  return options;
}

// create chat link and copy it, and scrape `CompanyLinkedIn` URL of selected Job
function createLinkAndCopy() {
  document.querySelector("#createChatDiv").style.display = "none";
  document.querySelector("#loadingDiv").style.display = "block";
  document.querySelector("#create-loading").style.display = "none";
  document.querySelectorAll("#create-loading .material-icons").forEach(elm => elm.style.opacity = 0);
  document.querySelector("#template-loading").style.display = "block";
  // request scraping of `CompanyLinkedIn` URL of selected Job
  let companyAboutURL = document
    .querySelector("#Job")
    .options[document.querySelector("#Job").selectedIndex].getAttribute(
      "CompanyLinkedIn"
    )
    .endsWith("/")
    ? document
        .querySelector("#Job")
        .options[document.querySelector("#Job").selectedIndex].getAttribute(
          "CompanyLinkedIn"
        ) + appendAbout
    : document
        .querySelector("#Job")
        .options[document.querySelector("#Job").selectedIndex].getAttribute(
          "CompanyLinkedIn"
        ) + "/" + appendAbout;
  const evt = document.querySelector("#Event");
  const sameEventType = evt.options[evt.selectedIndex].value === (dbEventType || NOEVENT);
  const evtVal = evt.options[evt.selectedIndex].value;
  const ENDPOINT = existing && sameEventType ? `${EDIT_CHAT}/${chats[0].Id}` : CREATE_CHAT;
  console.log("~~~~>", {dbEventType,ENDPOINT,evtVal})
  const notes = document.querySelector("#Notes").value;
  fetch(`${BASE_URL}/${ENDPOINT}`, {
    method: "post",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      ...linkedinData,
      Name: {
        First: document.querySelector("#FirstName").value,
        Last: document.querySelector("#LastName").value,
      },
      Email: getEmail(),
      PossibleEmails: getPossibleEmails(),
      PhoneNumber: document.querySelector("#PhoneNumber").value,
      JobId: parseInt(document.querySelector("#Job").value),
      LinkedInURL: encodeURI(document.querySelector("#LinkedInURL").value),
      SourceType: "Prospecting",
      CurrentLocation: document.querySelector("#CurrentLocation").value,
      CurrentPosition: document.querySelector("#CurrentPosition").value,
      EventType: evtVal === NOEVENT ? null : evtVal,
      Notes: notes
    }),
  })
    .then(checkResponse)
    .then(function (chat) {
      console.log("setting chat: ->", chat);
      dbEventType = chat.EventType
      existing = true
      document.querySelector("#loading-prospect .material-icons").style.opacity = 1;
      chats = [chat];
      if(companyAboutURL !== ("/" + appendAbout)){
        log(
          "sending requestCompanyData from create chat request -> " +
            companyAboutURL
        );
        chrome.runtime.sendMessage({
          type: "requestCompanyData",
          companyAboutURL: companyAboutURL,
        });
      }else{
        gotCompanyData();
      }

      document.querySelector("#createChatLinkBtn").disabled = true;
      document.querySelector("#chatLink").value = chat.URL;
      copyLink("chatLink")();
      document.querySelector("#copiedAfterCreateConfirmation").style.display =
        "block";

      if (document.querySelector("#resume").files.length) {
        uploadResume(chat.Id);
      }
    })
    .catch(function (error) {
      log(error);
    });
}

function gotDataTimeout(){
  console.log("gotDataTimeout", companyDataTimeLimit);
  clearTimeout(companyDataTimerId);
  companyDataTimerId = setTimeout(() => {
    console.info("company data timed out", linkedinData);
    chrome.runtime.sendMessage({ type: "linkedinData", data: linkedinData ? linkedinData : {} });
    showCreateChatDiv();
    const id = "error-" + Date.now() + "--" + Math.random();
    sendError(`${BASE_URL}/${ERRORS}`, id, JSON.stringify({
      seenUrls,
      linkedinData,
      TABURL,
      msg: "linked in scraper timed out"
    }));
  }, companyDataTimeLimit);
}

// check if chat for current LinkedIn URL alrady exists and show its link to user if so
function checkForDuplicate(data) {
  fetch(`${BASE_URL}/${FIND_CHAT}${encodeURI(data.LinkedInURL)}`, {
    method: "get",
    mode: "cors",
  })
    .then(checkResponse)
    .then(function (res) {
      if (res.length) {
        chats = res;
        existing = true;
        document.querySelectorAll("#create-loading .material-icons").forEach(x => x.style.opacity = 1);
        setTimeout(() => {
          document.querySelector("#create-loading").style.display = "none";
          document.querySelectorAll("#create-loading .material-icons").forEach(x => x.style.opacity = 0);
          document.querySelector("#template-loading").style.display = "block";
          document.querySelector("#loading-prospect").style.display = "none";
          log("requestCompanyData ->", chats[0].CompanyLinkedIn);
          if(chats[0].CompanyLinkedIn){
            let companyAboutURL = chats[0].CompanyLinkedIn.endsWith("/")
              ? chats[0].CompanyLinkedIn + appendAbout
              : chats[0].CompanyLinkedIn + "/" + appendAbout;
            chrome.runtime.sendMessage({
              type: "requestCompanyData",
              companyAboutURL: companyAboutURL,
            });
          }else{
            log("no company data");
            gotCompanyData();
          }
        }, 500)
      } else {
        existing = false;
        document.querySelector("#loading-job-company .material-icons").style.opacity = 1;
        setTimeout(showCreateChatDiv, 500);
      }
    })
    .catch(function (error) {
      log(error);
    });
}

// report errors
function reportErrors(url, errors, linkedInVersion) {
  fetch(`${BASE_URL}/${REPORT_ERRORS}`, {
    method: "post",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      URL: url,
      Errors: errors,
      LinkedInVersion: linkedInVersion,
    }),
  })
    .then(checkResponse)
    .then(function (response) {
      log(response);
    })
    .catch(function (error) {
      log(error);
    });
}

// upload resume
function uploadResume(chatID) {
  let formData = new FormData();

  formData.append("Resume", document.querySelector("#resume").files[0]);

  fetch(`${BASE_URL}/${UPLOAD_RESUME}/${chatID}`, {
    method: "post",
    mode: "cors",
    body: formData,
  })
    .then(checkResponse)
    .then(function (response) {
      log(response);
    })
    .catch(function (error) {
      log(error);
      alert("Resume Upload failed.");
    });
}

// helper function to check response from API for errors
function checkResponse(response) {
  if (response.status === 401) {
    showLoginDiv();
    throw response;
  } else if (!response.ok) {
    throw response;
  } else {
    return tryToGetJson(response);
  }
}

async function tryToGetJson(resp) {
  return new Promise((resolve) => {
    if (resp) {
      resp
        .json()
        .then((json) => resolve(json))
        .catch(() => resolve(null));
    } else {
      resolve(null);
    }
  });
}

const errorKey = "chatstrike-error-2";

const getStoredErrors = () => {
  const errs = localStorage.getItem(errorKey);
  return errs == null || errs == "" ? {} : JSON.parse(errs);
};

const sendErrors = (endpoint) => {
  const store = getStoredErrors();
  for (const k of Object.keys(store)) {
    // send outstanding errors one at a time
    sendError(endpoint, k, JSON.stringify(store[k]));
  }
};

const sendError = (endpoint, id, msg) => {
  const formam = new FormData();
  formam.append("ErrorId", id);
  formam.append("Error", msg);

  // actually send the error to the backend
  fetch(endpoint, {
    method: "POST",
    cache: "no-cache",
    body: formam,
  }).then(() => {
    const store = getStoredErrors();

    // since the send was successful remove it from the local cache of outstanding errors
    delete store[id];
    localStorage.setItem(errorKey, JSON.stringify(store));
  });
};

const maxWrites = 5;
var currentWrite = 0;

const writeErrorLog = (endpoint, msg, url, line, col, err) => {
  currentWrite++;
  log("ERR count: " + currentWrite);

  if (currentWrite >= maxWrites) {
    console.warn("Max frontend error logging reached for this session");
  } else {
    const store = getStoredErrors();

    // key for depulication of errors
    const id = "error-" + Date.now() + "--" + Math.random();

    // construct the error record
    // set it in the store
    store[id] = {
      date: Date.now(),
      agent: navigator.userAgent,
      msg: msg,
      url: url,
      line: line,
      col: col,
      err: err,
    };

    // record the error locally before sending incase sending fails
    localStorage.setItem(errorKey, JSON.stringify(store));

    // send any outstanding errors
    sendErrors(endpoint);
  }
};

const logAllErrors = (endpoint) => {
  // send any outstanding errors right away
  sendErrors(endpoint);

  // listen for errors, log and send them
  window.onerror = (msg, url, line, col, err) => {
    writeErrorLog(endpoint, msg + "", url, line, col, err);
  };
};

window.getBlackBox = () => {
  log(
    "Thank you for helping us get better! Please copy paste the follow to support@chatstrike.com:"
  );
  log(localStorage.getItem(errorKey));
};


var href = "";
chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
  href = tabs[0].url;
  restoreState();
});

function restoreState(){
  log("restoring state");
  state = getState();
  for (const k of Object.keys(state)) {
    const elm = document.getElementById(k);
    if(!elm.value){
      elm.value = state[k];
      elm.dispatchEvent(new Event("input"));
      elm.dispatchEvent(new Event("change"));
    }
  }
};

function setState(k, v){
  const state = getState();
  state[k] = v;
  localStorage.setItem(href, JSON.stringify(state));
};

function getState(){
  console.log("HREF", href);
  const item = localStorage.getItem(href);
  return item == null || item == "" ? {} : JSON.parse(item);
};

function setupStateListeners(){
  setTimeout(() => {
    for (const inp of document.querySelectorAll("input")) {
      inp.addEventListener("input", (e) => {
        setState(e.target.id, e.target.value);
      });
    }
    for (const inp of document.querySelectorAll("select")) {
      inp.addEventListener("input", (e) => {
        setState(e.target.id, e.target.value);
      });
    }
    restoreState();
  }, 200);
};

logAllErrors(`${BASE_URL}/${ERRORS}`);
